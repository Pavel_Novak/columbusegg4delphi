from invoke import task
from colorama import init, Fore, Back, Style
import os

init(autoreset=True)

def header(func = None, text = None):    
    if text == None:
        text = func.__doc__.upper()
    header_width = 80
    print(Fore.RED + Back.WHITE+ ' ' * header_width)
    print(Fore.RED + Back.WHITE + text.center(header_width))
    print(Fore.RED + Back.WHITE+ ' ' * header_width)


@task
def clean(ctx):
    """Cleans 'unittests' and 'samples' folders"""
    header(clean)
    
    cmd="del /s *.~*;*.exe;*.cfg;*.dof;*.gdb;*.dcu;*.local;*.identcache;*.dSYM;*.o;*.rsm;*.so;*.apk;*.log;*.tvsconfig;*.map"
    with ctx.prefix("cd unittests"):
        print('Cleaning... unittests')
        ctx.run(cmd, hide=True)
    
    with ctx.prefix("cd samples"):
        print('Cleaning... samples')
        ctx.run(cmd, hide=True)
   
@task()
def runtests(ctx):
    """Runs the unit tests"""
    header(runtests)
    unittestexe = r"unittests\bin\ColumbusUnitTests.exe"
    r = None
    if os.path.isfile(unittestexe):
        r = ctx.run(unittestexe)            

@task(pre=[clean])
def buildtests(ctx, run = False):
    """Compile the unit tests"""
    header(buildtests)
    r = None
    with ctx.prefix("rsvars.bat"):
        r = ctx.run(r"msbuild /t:Build /p:Config=CONSOLE /p:Platform=Win32 unittests\ColumbusUnitTests.dproj") #, hide=True)    

@task(clean, buildtests, runtests)
def tests(ctx):
    """Executes the unit tests"""
    header(text="BUILD OK")
    pass